package realstate.main.dev.data_objects;

import java.util.Map;

public class OfferDTO {
    public final String offerId;
    public final Map<String,Object> offerData;

    public OfferDTO(String offerId, Map<String,Object> offerData) {
        this.offerId = offerId;
        this.offerData = offerData;
    }
}
