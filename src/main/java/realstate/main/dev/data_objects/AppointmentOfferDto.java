package realstate.main.dev.data_objects;

public class AppointmentOfferDto {
    public final String appointmentId;
    public final String appointmentUserId;


    public AppointmentOfferDto(String appointmentId, String appointmentUserId) {
        this.appointmentId = appointmentId;
        this.appointmentUserId = appointmentUserId;
    }
}
