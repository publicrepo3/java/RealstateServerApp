package realstate.main.dev.data_objects;

public class AppointmentResponseDto {
    final public String appointmentId;
    public String date;
    public String time;
    public String dateTime;
    public String offerId;
    final public Object isApproved;

    public AppointmentResponseDto(String appointmentId, String date, String time, String dateTime,
                                  String offerId, Object isApproved) {
        this.appointmentId = appointmentId;
        this.date = date;
        this.time = time;
        this.dateTime = dateTime;
        this.offerId = offerId;
        this.isApproved = isApproved;
    }


}
