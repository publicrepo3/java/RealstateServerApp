package realstate.main.dev.api_services;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.cloud.storage.Bucket;
import realstate.main.dev.app.core.global_services.database_service.DatabaseCollections;
import realstate.main.dev.app.core.global_services.database_service.DatabaseDocuments;
import realstate.main.dev.app.core.global_services.ServicesLocator;
import realstate.main.dev.entities.Student;


import static realstate.main.dev.app.core.helpers.JsonHelpers.getStringJson;

public class StudentService {
    private final ConcurrentMap<Integer, Student> students;
    private final DocumentReference docRef;
    private final Bucket storage;
    private final CollectionReference colRef;
    private final AtomicInteger key;

    public StudentService() {
        this.storage = ServicesLocator.databaseService.storage;
        this.students = new ConcurrentHashMap<>();
        this.docRef = ServicesLocator.databaseService.firestore
                .collection(DatabaseCollections.usersCollection)
                .document(DatabaseDocuments.adminUserDoc);
        this.colRef = ServicesLocator.databaseService.firestore
                .collection(DatabaseCollections.usersCollection);
        key = new AtomicInteger();

        this.addStudent(new Student("Marisa", 10, "M"));
        this.addStudent(new Student("Ryan", 10, "A"));
        this.addStudent(new Student("John", 10, "K"));
    }

    public String findAllStudents(String clientId) throws ExecutionException, InterruptedException {
        try {
            List<Student> list = new ArrayList<Student>(this.students.values());

            List<Object> finalList = list.stream().map(Student::toJson).collect(Collectors.toList());

            return this.toJson(getUsersAsJsonString(), clientId);
        } catch (ExecutionException | InterruptedException e) {

            e.printStackTrace();
            return "No data";
        }
    }


    public String createStudent(String jsonPayload) {
//        if (jsonPayload == null) return false;
//
//        Gson gson = new Gson();
//        try {
//            Student student = gson.fromJson(jsonPayload, Student.class);
//            if (student != null) {
//                return this.addStudent(student);
//            }
//        } catch (Exception ignored) {
//        }
//        return false;
        return "";
    }


    private Object getUsersAsJsonString() throws ExecutionException, InterruptedException {
        Object usersJson = null;
        try {
            ApiFuture<QuerySnapshot> getDataFuture = colRef.get();
            List<QueryDocumentSnapshot> documents = getDataFuture.get().getDocuments();
            if (!documents.isEmpty()) {
                usersJson = documents.stream().map(QueryDocumentSnapshot::getData).collect(Collectors.toList());
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return usersJson;
    }


    private String toJson(Object list, String clientIp) {
        return getStringJson(list);
    }

    private boolean addStudent(Student student) {
        Integer id = key.incrementAndGet();
        student.setId(id);
        this.students.put(id, student);
        return true;
    }
}
