package realstate.main.dev.api_services.apointmetns;

import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import realstate.main.dev.api_services.authenfication.AuthService;
import realstate.main.dev.app.core.global_services.ServicesLocator;
import realstate.main.dev.app.core.global_services.database_service.DatabaseCollections;
import realstate.main.dev.app.core.utils.GeneralUtils;
import realstate.main.dev.app.core.utils.MultiFormRequestValidator;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.data_objects.AppointmentResponseDto;
import realstate.main.dev.entities.AppointmentEntity;
import realstate.main.dev.entities.ParamEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;


public class AppointmentsService {

    static private final Firestore fireStore = ServicesLocator.databaseService.firestore;

    public static void makeAppointments(HttpServletResponse response, HttpServletRequest request) throws ExecutionException,
            InterruptedException {
        var userId = request.getParameter(AppointmentsRequestsParams.userId);
        var offerId = request.getParameter(AppointmentsRequestsParams.offerId);
        var date = request.getParameter(AppointmentsRequestsParams.appointmentDate);
        var time = request.getParameter(AppointmentsRequestsParams.appointmentTime);

        if (MultiFormRequestValidator.validateGeneralRequest(response, List.of(
                new ParamEntity(AppointmentsRequestsParams.userId, userId),
                new ParamEntity(AppointmentsRequestsParams.offerId, offerId),
                new ParamEntity(AppointmentsRequestsParams.appointmentDate, date),
                new ParamEntity(AppointmentsRequestsParams.appointmentTime, time)
        ))) {

            ///Check the offer is already exist in database
            if (ApiServicesHelper.getOfferObject(offerId).get().get().exists()) {
                ///Check the conflict
                if (checkNoConflictedAppointments(userId, String.join(" ", date, time),
                        getQueryDocumentSnapshots(DatabaseCollections.appointmentsCollection))) {
                    ///Add to appointments collections
                    var appointmentData = fireStore
                            .collection(DatabaseCollections.appointmentsCollection)
                            .add(new AppointmentEntity(
                                    userId, offerId,
                                    date, time,
                                    false)
                                    .toMap())
                            .get();

                /*    var appointmentsArray = ApiServicesHelper.getOfferObject(offerId).update(AppointmentsRequestsParams.appointmentData,
                            FieldValue.arrayUnion(
                                    Map.of(AppointmentsRequestsParams.userId, userId,
                                            AppointmentsRequestsParams.appointmentId, appointmentData.getId())
                            ));*/

                    RequestResponseUtils.outputResponse(response,
                            appointmentData.getId(),
                            HttpServletResponse.SC_OK);
                } else
                    RequestResponseUtils.outputResponse(response,
                            "Appointments time conflicts",
                            HttpServletResponse.SC_FORBIDDEN);
            } else
                RequestResponseUtils.outputResponse(response,
                        "Not exist offer",
                        HttpServletResponse.SC_FORBIDDEN);
        }
    }

    public static void removeAppointmentService(HttpServletResponse resp, String userId,
                                                String appointmentId) {
        if (AuthService.validateGivenToken(resp, userId)) {
            try {
                DocumentSnapshot appointment = ApiServicesHelper
                        .getDocument(DatabaseCollections.appointmentsCollection, appointmentId);
                if (appointment.exists()) {
///If is the correct user
                    if (Objects.equals(appointment
                            .get(AppointmentsService.AppointmentsRequestsParams.userId), userId)) {
                        ApiServicesHelper.getCollection(DatabaseCollections.appointmentsCollection)
                                .document(appointmentId)
                                .delete();
                        RequestResponseUtils.outputResponse(resp, "Deleted " +
                                "Successfully", HttpServletResponse.SC_OK);
                    } else
                        RequestResponseUtils.outputResponse(resp, "Not yours",
                                HttpServletResponse.SC_FORBIDDEN);
                } else
                    RequestResponseUtils.outputResponse(resp, "Not exist",
                            HttpServletResponse.SC_FORBIDDEN);
            } catch (Exception e) {
                RequestResponseUtils.outputResponse(resp,
                        e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
                e.printStackTrace();
            }
        }
    }

    private static List<QueryDocumentSnapshot> getQueryDocumentSnapshots(String collection) throws InterruptedException, ExecutionException {
        return fireStore
                .collection(collection)
                .get()
                .get()
                .getDocuments();
    }

    public static void getAppointmentByOfferId(HttpServletResponse resp,
                                               AppointmentsGettersParams appointParams) {
        try {
            var listOfUserAppointments =
                    ApiServicesHelper.getCollection(DatabaseCollections.appointmentsCollection)
                            .offset(Integer.parseInt(appointParams.offset))
                            .limit(Integer.parseInt(appointParams.limit))
                            .get()
                            .get()
                            .getDocuments()
                            .stream().filter(e -> Objects
                                    .equals(e.get(AppointmentsService.AppointmentsRequestsParams.offerId), appointParams.offerId))
                            .collect(Collectors.toList());
            prepareListOffAppointments(resp, listOfUserAppointments);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(),
                    HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }

    private static List<AppointmentResponseDto> prepareListOffAppointments(HttpServletResponse resp, List<QueryDocumentSnapshot> listOfUserAppointments) {
        var finalRes =
                listOfUserAppointments.stream().map(
                        e -> new AppointmentResponseDto(
                                e.getId(),
                                e.getString(AppointmentsRequestsParams.appointmentDate),
                                e.getString(AppointmentsRequestsParams.appointmentTime),
                                e.getString(AppointmentsRequestsParams.appointmentDateTime),
                                e.getString(AppointmentsRequestsParams.offerId),
                                (e.get(AppointmentsRequestsParams.isAdminApproved)))
                ).collect(Collectors.toList());
        RequestResponseUtils.outputResponse(resp, finalRes,
                HttpServletResponse.SC_OK);
        return finalRes;
    }

    public static void getAppointmentByUserIdOfferId(HttpServletResponse resp, String userId, String offerId, String limitTo, String offset) {
        try {
            var listOfUserAppointments =
                    ApiServicesHelper.getCollection(DatabaseCollections.appointmentsCollection)
                            .offset(Integer.parseInt(offset))
                            .limit(Integer.parseInt(limitTo))
                            .get()
                            .get()
                            .getDocuments()
                            .stream().filter(e -> Objects
                                    .equals(e.get(AppointmentsService.AppointmentsRequestsParams.offerId), offerId))
                            .filter(e -> Objects
                                    .equals(e.get(AppointmentsService.AppointmentsRequestsParams.userId), userId))
                            .collect(Collectors.toList());
            prepareListOffAppointments(resp, listOfUserAppointments);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(),
                    HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }

    private static boolean checkNoConflictedAppointments(String userId, String dateTime, List<QueryDocumentSnapshot> documents) {

        return documents.stream().noneMatch(
                e -> Objects.equals(e.get(AppointmentsRequestsParams.appointmentDateTime), dateTime)
                        && Objects.equals(e.getString(AppointmentsRequestsParams.userId), userId));

    }

    public static void getAppointmentsByUserId(HttpServletResponse resp, String userId, String limitTo, String offset) {
        try {
            var listOfUserAppointments =
                    ApiServicesHelper.getCollection(DatabaseCollections.appointmentsCollection)
                            .offset(Integer.parseInt(offset))
                            .limit(Integer.parseInt(limitTo))
                            .get()
                            .get()
                            .getDocuments()
                            .stream().filter(e -> Objects
                                    .equals(e.get(AppointmentsService.AppointmentsRequestsParams.userId), userId))
                            .collect(Collectors.toList());
            prepareListOffAppointments(resp, listOfUserAppointments);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(),
                    HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }


    public static class AppointmentsGettersParams {
        public String offset;
        public String limit;
        public String offerId;
        public String userId;


    }

    public static class AppointmentsRequestsParams {
        public static final String userId = "userId";
        public static final String offerId = "offerId";
        public static final String appointmentDate = "appointmentDate";
        public static final String appointmentTime = "appointmentTime";
        public static final String appointmentId = "appointmentId";
        public static final String isAdminApproved = "isAdminApproved";
        public static final String appointmentDateTime = "appointmentDateTime";
        public static final String appointmentData = "appointmentData";
    }


    static public class Admin {
        public static void getAppointmentsByAdmin(HttpServletResponse resp,
                                                  String offset, String limit,
                                                  String userId) {

            if (AuthService.validateGivenTokenForAdmin(resp, userId)) {
                try {
                    var listOfUserAppointments =
                            ApiServicesHelper.getCollection(DatabaseCollections.appointmentsCollection)
                                    .offset(Integer.parseInt(offset))
                                    .limit(Integer.parseInt(limit))
                                    .get()
                                    .get()
                                    .getDocuments();
                    prepareListOffAppointments(resp, listOfUserAppointments);
                } catch (Exception e) {
                    RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(),
                            HttpServletResponse.SC_EXPECTATION_FAILED);
                    e.printStackTrace();
                }
            }

        }


        public static void approveAppointmentService(HttpServletResponse resp,
                                                     String userId,
                                                     String appointmentId,
                                                     String isAdminApproved) {
            if (AuthService.validateGivenTokenForAdmin(resp, userId)) {
                try {
                    DocumentSnapshot appointment = ApiServicesHelper
                            .getDocument(DatabaseCollections.appointmentsCollection, appointmentId);
                    if (appointment.exists()) {

                        ApiServicesHelper.getCollection(DatabaseCollections.appointmentsCollection)
                                .document(appointmentId)
                                .update(AppointmentsRequestsParams.isAdminApproved,
                                        GeneralUtils.isaTrue(isAdminApproved.trim()));
                        RequestResponseUtils.outputResponse(resp, "Updated " +
                                "Successfully", HttpServletResponse.SC_OK);

                    } else
                        RequestResponseUtils.outputResponse(resp, "Not exist",
                                HttpServletResponse.SC_FORBIDDEN);
                } catch (Exception e) {
                    RequestResponseUtils.outputResponse(resp,
                            e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
                    e.printStackTrace();
                }
            }
        }


    }
}
