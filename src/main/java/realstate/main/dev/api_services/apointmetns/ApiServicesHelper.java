package realstate.main.dev.api_services.apointmetns;

import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import realstate.main.dev.app.core.global_services.ServicesLocator;
import realstate.main.dev.app.core.global_services.database_service.DatabaseCollections;

import java.util.Objects;
import java.util.concurrent.ExecutionException;

public class ApiServicesHelper {
    static private final Firestore fireStore = ServicesLocator.databaseService.firestore;

    public static String getAppointmentUserId(DocumentReference appointment) throws
            InterruptedException, ExecutionException {
        return Objects.requireNonNull(
                appointment
                        .get()
                        .get()
                        .get(AppointmentsService.AppointmentsRequestsParams.userId)).toString().trim();
    }

    public static Object getOfferObjectField(String offerId, String filed) throws InterruptedException,
            ExecutionException {
        return ApiServicesHelper.getOfferObject(offerId)
                .get()
                .get()
                .get(filed);
    }

    public static DocumentReference getAppointment(String appointmentIdInOffer) {
        return Objects.requireNonNull(
                fireStore.collection(DatabaseCollections.appointmentsCollection)
                        .document(appointmentIdInOffer));
    }


    public static DocumentReference getOfferObject(String offerId) {

        return Objects.requireNonNull(
                fireStore.collection(DatabaseCollections.userOffersCollection)
                        .document(offerId));
    }

    public static CollectionReference getCollection(String colRef) {
        return fireStore.collection(colRef);
    }

    public static DocumentSnapshot getDocument(String collectionName,
                                               String docName) throws ExecutionException, InterruptedException {
        return fireStore.collection(collectionName).document(docName).get().get();
    }
}
