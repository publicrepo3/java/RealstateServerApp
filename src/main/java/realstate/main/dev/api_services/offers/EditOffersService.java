package realstate.main.dev.api_services.offers;

import realstate.main.dev.api_services.authenfication.AuthService;
import realstate.main.dev.app.core.global_services.database_service.DatabaseFields;
import realstate.main.dev.app.core.utils.MultiFormRequestValidator;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.entities.ParamEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class EditOffersService {

    public static void acceptOfferByUser(HttpServletResponse response,
                                         HttpServletRequest request) {

        try {
            var isAvailable =
                    request.getParameter(CreateOfferService.OfferProperties.offerIsAvailable);
            var offerId = request.getParameter(CreateOfferService.OfferProperties.offerId);
            var userId = request.getParameter(DatabaseFields.userId);
            if (AuthService.validateGivenToken(response, userId))
                if (MultiFormRequestValidator.validateGeneralRequest(response, List.of(
                        new ParamEntity(CreateOfferService.OfferProperties.offerId, offerId),
                        new ParamEntity(CreateOfferService.OfferProperties.offerIsAvailable,
                                isAvailable)
                )))
                    OffersGeneralServices.acceptOfferByUser(response,
                            isAvailable,
                            offerId, userId);


        } catch (
                Exception e) {
            RequestResponseUtils.outputResponse(response, e.getMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }

    public static void approveOfferByAdmin(HttpServletResponse response, HttpServletRequest request) {

        try {
            var isAdminApproved = request.getParameter(CreateOfferService.OfferProperties.isAdminApproved);
            var offerId = request.getParameter(CreateOfferService.OfferProperties.offerId);
            if (MultiFormRequestValidator.validateGeneralRequest(response, List.of(
                    new ParamEntity(CreateOfferService.OfferProperties.offerId, offerId),
                    new ParamEntity(CreateOfferService.OfferProperties.isAdminApproved, isAdminApproved)
            )))
                OffersGeneralServices.approveOffer(response,
                        isAdminApproved,
                        offerId);


        } catch (
                Exception e) {
            RequestResponseUtils.outputResponse(response, e.getMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }

}
