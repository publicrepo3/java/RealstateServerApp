package realstate.main.dev.api_services.offers;


import realstate.main.dev.app.core.utils.MultiFormRequestValidator;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.entities.ParamEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;

import static realstate.main.dev.app.core.utils.GeneralUtils.isaTrue;

public class CreateOfferService {
    public static void offerProcessor(HttpServletResponse response,
                                      HttpServletRequest request,
                                      OfferProcessingType offerProcessingType) {
        try {
            var offerImagePart = request.getPart(OfferProperties.offerImage);
            var certificatePart = request.getPart(OfferProperties.certificate);
            var userId = request.getParameter(OfferProperties.offerUserId);
            var forRent = request.getParameter(OfferProperties.forRent);
            var forSale = request.getParameter(OfferProperties.forSale);
            var offerTitle = request.getParameter(OfferProperties.offerTitle);

            var offerAddressDetails = request.getParameter(OfferProperties.offerAddressDetails);
            var offerId = request.getParameter(OfferProperties.offerId);
            ///Offer search details
            var space = request.getParameter(OfferProperties.offerSpace);
            var price = request.getParameter(OfferProperties.offerPrice);
            var roomsCount = request.getParameter(OfferProperties.offerRoomsCount);
            var floor = request.getParameter(OfferProperties.offerFloors);
            var cladding = request.getParameter(OfferProperties.offerCladding);

            ///
            if (MultiFormRequestValidator.validateMultiPartRequest(response, new ArrayList<>(Arrays.asList(
                    offerImagePart, certificatePart
            )), offerProcessingType == OfferProcessingType.creatState ? Arrays.asList(
                    new ParamEntity(OfferProperties.offerUserId, userId),
                    new ParamEntity(OfferProperties.forRent, forRent),
                    new ParamEntity(OfferProperties.forSale, forSale),
                    new ParamEntity(OfferProperties.offerTitle, offerTitle),
                    new ParamEntity(OfferProperties.offerAddressDetails, offerAddressDetails)
            ) : Arrays.asList(
                    new ParamEntity(OfferProperties.offerUserId, userId),
                    new ParamEntity(OfferProperties.forRent, forRent),
                    new ParamEntity(OfferProperties.forSale, forSale),
                    new ParamEntity(OfferProperties.offerTitle, offerTitle),
                    new ParamEntity(OfferProperties.offerAddressDetails, offerAddressDetails),
                    new ParamEntity(OfferProperties.offerId, offerId)


            ))) {
                OffersGeneralServices.offersProcessor(response, offerImagePart, certificatePart,
                        userId, offerTitle,
                        offerAddressDetails,
                        isaTrue(forSale), isaTrue(forRent), offerId
                        , space, roomsCount, price, floor, cladding);

            }
        } catch (
                Exception e) {
            RequestResponseUtils.outputResponse(response, e.getMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }

    public static class OfferProperties {
        static public final String offerImage = "offerImage";
        static public final String certificate = "offerCertificate";
        static public final String offerUserId = "offerUserId";
        static public final String isAdminApproved = "isAdminApproved";
        static public final String forRent = "forRent";
        static public final String forSale = "forSale";
        static public final String offerTitle = "offerTitle";
        static public final String offerDesc = "offerDesc";
        static public final String offerId = "offerId";
        static public final String offerIsAvailable = "isAvailable";
        static public final String offerAddressDetails = "offerAddressDetails";
        static public final String offerSpace = "offerSpace";
        static public final String offerPrice = "offerPrice";
        static public final String offerRoomsCount = "offerRoomsCount";
        static public final String offerFloors = "offerFloor";
        static public final String offerCladding = "offerCladding";
    }

    public enum OfferProcessingType {
        editState,
        creatState
    }


}
