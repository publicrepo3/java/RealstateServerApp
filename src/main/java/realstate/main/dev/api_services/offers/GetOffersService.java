package realstate.main.dev.api_services.offers;

import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import realstate.main.dev.app.core.global_services.ServicesLocator;
import realstate.main.dev.app.core.global_services.database_service.DatabaseCollections;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.data_objects.OfferDTO;
import realstate.main.dev.entities.OfferSearchEntity;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class GetOffersService {
    static private final Firestore fireStore = ServicesLocator.databaseService.firestore;

    public static void getOffersByUserId(HttpServletResponse response, String userId) {
        try {

            var offersList = fireStore.collection(DatabaseCollections.userOffersCollection).get()
                    .get()
                    .getDocuments()
                    .stream().map(e -> new OfferDTO(e.getId(), e.getData()))
                    .filter(e -> e.offerData.containsValue(userId))
                    .collect(Collectors.toList());
            RequestResponseUtils.outputResponse(response, offersList, HttpServletResponse.SC_OK);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }

    }

    public static void getOffersByOfferId(HttpServletResponse response, String offerId) {
        try {
            var offersList = fireStore.collection(DatabaseCollections.userOffersCollection).get()
                    .get()
                    .getDocuments()
                    .stream().map(e -> new OfferDTO(e.getId(), e.getData()))
                    .filter(t -> t.offerId.equals(offerId))
                    .collect(Collectors.toList());
            offersList.forEach(e -> e.offerData.remove(CreateOfferService.OfferProperties.offerUserId));
            RequestResponseUtils.outputResponse(response, offersList, HttpServletResponse.SC_OK);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }


    public static void getAllOffers(HttpServletResponse response, int offset, int limit) {
        try {

            var offersList = fireStore.collection(DatabaseCollections.userOffersCollection)
                    .offset(offset).limit(limit)
                    .get()
                    .get()
                    .getDocuments()
                    .stream().map(e -> new OfferDTO(e.getId(), e.getData()))
                    .collect(Collectors.toList());
            RequestResponseUtils.outputResponse(response, offersList, HttpServletResponse.SC_OK);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }

    }

    public static void getAllOffersForSearch(
            HttpServletResponse response, int offset, int limit, OfferSearchEntity offerSearchEntity
    ) {
        try {


            List<QueryDocumentSnapshot> offerDocs = fireStore.collection(DatabaseCollections.userOffersCollection)
                    .offset(offset).limit(limit)
                    .get()
                    .get()
                    .getDocuments();


            var finalOfferList = getFinalOfferListFromListOfKeyValues(
                    new ConcurrentHashMap<>() {{
                        if (offerSearchEntity.space != null)
                            putIfAbsent(CreateOfferService.OfferProperties.offerSpace,
                                    offerSearchEntity.space);
                        if (offerSearchEntity.price != null)
                            putIfAbsent(CreateOfferService.OfferProperties.offerPrice,
                                    offerSearchEntity.price);
                        if (offerSearchEntity.floor != null)
                            putIfAbsent(CreateOfferService.OfferProperties.offerFloors,
                                    offerSearchEntity.floor);
                        if (offerSearchEntity.roomsCount != null)
                            putIfAbsent(CreateOfferService.OfferProperties.offerRoomsCount,
                                    offerSearchEntity.roomsCount);
                        if (offerSearchEntity.cladding != null)
                            putIfAbsent(CreateOfferService.OfferProperties.offerCladding,
                                    offerSearchEntity.cladding);
                    }}, offerDocs);

            var finalSearchResult =
                    finalOfferList.stream()
                            .filter(e -> Objects.equals(e.get(CreateOfferService.OfferProperties.isAdminApproved), true)
                                    && Objects.equals(e.get(CreateOfferService.OfferProperties.offerIsAvailable), true))
                            .map(e -> new OfferDTO(e.getId(),
                                    e.getData()))
                            .collect(Collectors.toList());

            ///Response
            RequestResponseUtils.outputResponse(response, finalSearchResult, HttpServletResponse.SC_OK);

        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }

    }


    private static List<QueryDocumentSnapshot> getFinalOfferListFromListOfKeyValues
            (Map<String, Object> searchParams,
             List<QueryDocumentSnapshot> offerDocs) {
        var filteredOffers = offerDocs;
///How to iterate in map
        for (Map.Entry<String, Object> entry : searchParams.entrySet()) {

            if (!entry.getKey().isEmpty() && entry.getValue() != null) {
                if (!entry.getKey().equals(CreateOfferService.OfferProperties.offerCladding))
                    filteredOffers = filteredOffers.stream().filter(
                            e -> String.valueOf(e.get(entry.getKey())).toLowerCase(Locale.ROOT).trim()
                                    .equals(entry.getValue().toString().toLowerCase(Locale.ROOT).trim())
                    ).collect(Collectors.toList());

                else
                    filteredOffers = filteredOffers.stream().filter(
                            e -> String.valueOf(e.get(entry.getKey())).toLowerCase(Locale.ROOT).trim()
                                    .contains(entry.getValue().toString().toLowerCase(Locale.ROOT).trim())
                    ).collect(Collectors.toList());
            }

        }
        return filteredOffers;
    }

    public static void getOffersByUserIdOfferId(HttpServletResponse response, String userId, String offerId) {
        try {
            var offersList = fireStore.collection(DatabaseCollections.userOffersCollection).get()
                    .get()
                    .getDocuments()
                    .stream().map(e -> new OfferDTO(e.getId(), e.getData()))
                    .filter(e -> e.offerData.containsValue(userId) && Objects.equals(e.offerId, offerId))
                    .collect(Collectors.toList());

            offersList.forEach(e -> e.offerData.remove(CreateOfferService.OfferProperties.offerUserId));
            RequestResponseUtils.outputResponse(response, offersList, HttpServletResponse.SC_OK);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }


    public static void getApprovedAndAvailableOffers(HttpServletResponse response
    ) {
        try {
            var offersList = fireStore.collection(DatabaseCollections.userOffersCollection).get()
                    .get()
                    .getDocuments()
                    .stream().map(e -> new OfferDTO(e.getId(), e.getData()))
                    .filter(e -> Objects.equals(e.offerData.get(CreateOfferService.OfferProperties.isAdminApproved), true) &&
                            Objects.equals(e.offerData.get(CreateOfferService.OfferProperties.offerIsAvailable), true))
                    .collect(Collectors.toList());
            RequestResponseUtils.outputResponse(response, offersList, HttpServletResponse.SC_OK);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }
}
