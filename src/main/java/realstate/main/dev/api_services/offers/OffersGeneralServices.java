package realstate.main.dev.api_services.offers;

import com.google.cloud.firestore.Firestore;
import realstate.main.dev.api_services.apointmetns.ApiServicesHelper;
import realstate.main.dev.app.core.global_services.ServicesLocator;
import realstate.main.dev.app.core.global_services.database_service.DatabaseCollections;
import realstate.main.dev.app.core.global_services.storage_services.UploadFilesService;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.entities.OfferEntity;
import realstate.main.dev.entities.ServiceTypeEntity;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.net.URL;
import java.util.Objects;

import static realstate.main.dev.app.core.utils.GeneralUtils.isaTrue;

public class OffersGeneralServices {

    static private final Firestore fireStore = ServicesLocator.databaseService.firestore;

    static void offersProcessor(HttpServletResponse response, Part offerImagePart,
                                Part certificatePart, String userId,
                                String offerTitle,
                                String offerAddressDetails,
                                boolean forSale,
                                boolean forRent, String offerId, String space,
                                String roomsCount, String price, String floor,
                                String cladding) {
        try {
            URL offerImageUrl;
            URL certificateUrl;
            var imageFolderName = offerTitle + "/" + userId.substring(0, 10) + "/images";
            var certificatesFolderName = offerTitle + "/" + userId.substring(0, 10) + "/certificates";

            offerImageUrl = UploadFilesService
                    .uploadImages(response, offerImagePart, imageFolderName, offerImagePart.getSubmittedFileName(), false);

            certificateUrl = UploadFilesService
                    .uploadImages(response, certificatePart, certificatesFolderName, certificatePart.getSubmittedFileName(), false);

            var offerEntity = new OfferEntity(
                    offerImageUrl != null ? offerImageUrl.toURI().toString() : "",
                    certificateUrl != null ? certificateUrl.toURI().toString() : "",
                    userId, false,
                    new ServiceTypeEntity(forSale, forRent),
                    offerTitle, "".replaceAll("\"", ""),
                    isaTrue("true"),
                    offerAddressDetails, space, price, roomsCount, floor,
                    cladding);
            if (offerId == null)
                ServicesLocator.databaseService.firestore
                        .collection(DatabaseCollections.userOffersCollection)
                        .add(offerEntity.toMap());
            else {
                ///Editing process will escape all null fields
                ServicesLocator.databaseService.firestore
                        .collection(DatabaseCollections.userOffersCollection)
                        .document(offerId)
                        .update(offerEntity.toMap());
            }

            RequestResponseUtils.outputResponse(response, offerId == null ? "Offer has been added successfully" : "" +
                            "Offer has been edited successfully",
                    HttpServletResponse.SC_OK);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }


    static public boolean isForYouOffer(HttpServletResponse response, String offerId,
                                        String userId) {
        try {
            var offerUserId = Objects.requireNonNull(ApiServicesHelper.getOfferObject(offerId)
                    .get()
                    .get()
                    .get(CreateOfferService.OfferProperties.offerUserId)).toString();
            return offerUserId.equals(userId);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(),
                    HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
            return false;
        }
    }

    public static void approveOffer(HttpServletResponse response, String isAdminApproved, String offerId) {
        try {
            var result = fireStore.collection(DatabaseCollections.userOffersCollection).document(offerId)
                    .update(CreateOfferService.OfferProperties.isAdminApproved, isaTrue(isAdminApproved));
            RequestResponseUtils.outputResponse(response, "Updated successfully", HttpServletResponse.SC_OK);

        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }

    }


    public static void acceptOfferByUser(HttpServletResponse response,
                                         String isAccepted, String offerId,
                                         String userId) {
        try {
            var offerObject = ApiServicesHelper.getOfferObject(offerId);
            var offerData = offerObject.get().get();
            if (offerData.exists()) {
                if (Objects.equals(offerData.get(CreateOfferService.OfferProperties.offerUserId),
                        userId.trim())) {
                    var result = fireStore.collection(DatabaseCollections.userOffersCollection).document(offerId)
                            .update(CreateOfferService.OfferProperties.offerIsAvailable,
                                    isaTrue(isAccepted));
                    RequestResponseUtils.outputResponse(response, "Updated successfully",
                            HttpServletResponse.SC_OK);
                } else
                    RequestResponseUtils.outputResponse(response, "Not yours",
                            HttpServletResponse.SC_FORBIDDEN);
            } else
                RequestResponseUtils.outputResponse(response, "Not exist",
                        HttpServletResponse.SC_FORBIDDEN);


        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }

    }
}

