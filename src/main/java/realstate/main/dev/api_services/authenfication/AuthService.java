package realstate.main.dev.api_services.authenfication;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import com.google.gson.Gson;
import realstate.main.dev.app.core.global_services.ServicesLocator;
import realstate.main.dev.app.core.global_services.database_service.DatabaseCollections;
import realstate.main.dev.app.core.global_services.database_service.DatabaseFields;
import realstate.main.dev.app.core.utils.MultiFormRequestValidator;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.entities.LoginEntity;
import realstate.main.dev.entities.ParamEntity;
import realstate.main.dev.entities.UserEntity;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class AuthService {
    static private final FirebaseAuth auth = ServicesLocator.databaseService.auth;
    static private final Firestore firestore = ServicesLocator.databaseService.firestore;
    public static final String USER_PASSWORD = "userPassword";

    private static String verifyUser(HttpServletResponse response, String userEmail) {
        try {
            var validatedRequest = MultiFormRequestValidator.validateGeneralRequest(response, List.of(
                    new ParamEntity(AuthParams.userEmail, userEmail)
            ));
            if (validatedRequest) {
                UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                        .setEmail(userEmail);
                var userRecord = auth.createUserAsync(request);
                var token = auth.createCustomTokenAsync(userRecord.get().getUid());

                String trimedToken = token.get().trim();

                firestore.collection(DatabaseCollections.tokens).add(new ConcurrentHashMap<>() {{
                    putIfAbsent(AuthParams.userEmail, userEmail);
                    putIfAbsent(AuthParams.userToken, trimedToken);

                }});
                return trimedToken;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            RequestResponseUtils.outputResponse(response, e.getMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            return null;
        }
    }

    public static void loginUser(HttpServletResponse response, String jsonPayload) {

        try {
            final var loginEntity = new Gson().fromJson(jsonPayload, LoginEntity.class);

            var listOfPhonesAndPasswords = getUserCollectionQuerySnapshotApiFuture(DatabaseCollections.usersCollection)
                    .get().getDocuments().stream()
                    .map(QueryDocumentSnapshot::getData).collect(Collectors.toList()).stream()
                    .filter(e -> e.containsValue(loginEntity.userEmail) && e.containsValue(loginEntity.userPassword)).collect(Collectors.toList());
            if (listOfPhonesAndPasswords.isEmpty()) {
                RequestResponseUtils.outputResponse(response, "User phone number or " +
                        "password is not correct.", HttpServletResponse.SC_BAD_REQUEST);
            } else {
                var loginResponse = listOfPhonesAndPasswords.stream().findFirst().get();
                loginResponse.remove(USER_PASSWORD);
                RequestResponseUtils.outputResponse(response,
                        loginResponse,
                        HttpServletResponse.SC_OK);
            }

        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }

    public static void createUser(HttpServletResponse response, String jsonPayload) {

        UserEntity user = new Gson().fromJson(jsonPayload, UserEntity.class);
        try {
            var token = verifyUser(response, user.userEmail);
            if (token != null) {
                var validatedRequest = MultiFormRequestValidator.validateGeneralRequest(response, List.of(
                        new ParamEntity(AuthParams.userEmail, user.userEmail),
                        new ParamEntity(AuthParams.userName, user.userName)

                ));
                if (validatedRequest) {

                    var users = getUserCollectionQuerySnapshotApiFuture(DatabaseCollections.usersCollection).get().getDocuments();
                    var useIsExist = users.stream().anyMatch(e -> Objects.equals(e.get(DatabaseFields.userId, String.class), token));
                    if (!useIsExist) {
                        var verifiedTokens = getUserCollectionQuerySnapshotApiFuture(DatabaseCollections.tokens);
                        var tokens = verifiedTokens.get().getDocuments();
                        var userIsVerified = tokens.stream().anyMatch(e ->
                                Objects.equals(e.get(AuthParams.userToken, String.class), token.trim()));

                        if (userIsVerified) {
                            ConcurrentHashMap<String, Object> userData = new ConcurrentHashMap<>() {{
                                putIfAbsent(DatabaseFields.userName, user.userName);
                                putIfAbsent(DatabaseFields.userId, token);
                                putIfAbsent(DatabaseFields.userEmail, user.userEmail);
                                putIfAbsent(DatabaseFields.userPassword, user.userPassword);
                            }};
                            ApiFuture<DocumentReference> addUserFeature =
                                    firestore.collection(DatabaseCollections.usersCollection).add(userData);
                            var finalRes = Objects.requireNonNull(addUserFeature
                                    .get()
                                    .get()
                                    .get()
                                    .getData());
                            finalRes.remove(USER_PASSWORD);
                            RequestResponseUtils.outputResponse(response,
                                    finalRes,
                                    HttpServletResponse.SC_OK);
                        } else
                            RequestResponseUtils.outputResponse(response, "User is not verified", HttpServletResponse.SC_BAD_REQUEST);

                    } else
                        RequestResponseUtils.outputResponse(response, "User is already exist", HttpServletResponse.SC_BAD_REQUEST);
                }
            }

        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, "User is already exist", HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }

    }

    private static ApiFuture<QuerySnapshot> getUserCollectionQuerySnapshotApiFuture(String collection) {
        return firestore.collection(collection).get();
    }

    public static class AuthParams {

        static public String userEmail = "userEmail";
        static public String userName = "userName";
        static public String userRoleTypes = "userRoleTypes";
        static public String userToken = "token";
    }

    static public boolean validateGivenToken(HttpServletResponse response, String token) {
        try {
            if (getUserCollectionQuerySnapshotApiFuture(DatabaseCollections.tokens)
                    .get()
                    .getDocuments()
                    .stream().map(QueryDocumentSnapshot::getData)
                    .anyMatch(e -> e.containsValue(token)))
                return true;
            else {
                RequestResponseUtils.outputResponse(response, "Not authenticated", HttpServletResponse.SC_BAD_REQUEST);
                return false;
            }

        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
            return false;

        }
    }


    static public boolean validateGivenTokenForAdmin(HttpServletResponse response,
                                                     String token) {
        try {
            if (getUserCollectionQuerySnapshotApiFuture(DatabaseCollections.tokens)
                    .get()
                    .getDocuments()
                    .stream().anyMatch(e -> Objects.equals(e.get(DatabaseFields.token), token)))
                return true;
            else {
                RequestResponseUtils.outputResponse(response, "Not admin account",
                        HttpServletResponse.SC_BAD_REQUEST);
                return false;
            }

        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
            return false;

        }
    }
}
