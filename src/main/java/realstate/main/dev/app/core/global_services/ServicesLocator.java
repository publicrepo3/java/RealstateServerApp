package realstate.main.dev.app.core.global_services;

import realstate.main.dev.app.core.global_services.database_service.DatabaseStorageService;

import java.io.IOException;
import java.io.InputStream;

public class ServicesLocator {
    public static DatabaseStorageService databaseService = null;


    public static void initServiceLocator(InputStream keyPath) {
        try {
            databaseService = DatabaseStorageService.getInstance(keyPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
