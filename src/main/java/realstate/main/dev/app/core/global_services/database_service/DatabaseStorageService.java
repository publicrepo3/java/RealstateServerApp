package realstate.main.dev.app.core.global_services.database_service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.storage.Bucket;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.cloud.StorageClient;

import java.io.IOException;
import java.io.InputStream;

public class DatabaseStorageService {

    public Firestore firestore = null;
    public FirebaseAuth auth = null;
    public Bucket storage = null;
    private static InputStream KEY_PATH = null;

    public static DatabaseStorageService getInstance(InputStream inputStream) throws IOException {
        KEY_PATH = inputStream;
        if (single_instance == null) {
            single_instance = new DatabaseStorageService();
        }
        return single_instance;
    }

    public static final String DATABASE_LINK = "https://realstateserverapp-default-rtdb.firebaseio.com";
    public static final String STORAGE_BUCKET = "realstateserverapp.appspot.com";


    private DatabaseStorageService() throws IOException {
        try {
            initDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static DatabaseStorageService single_instance = null;


    private void initDatabase() throws IOException {
        try {

            FirebaseOptions options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(KEY_PATH))
                    .setDatabaseUrl(DATABASE_LINK)
                    .setStorageBucket(STORAGE_BUCKET)
                    .build();

            FirebaseApp.initializeApp(options);
            firestore = FirestoreClient.getFirestore();
            auth = FirebaseAuth.getInstance();
            storage = StorageClient.getInstance().bucket();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

