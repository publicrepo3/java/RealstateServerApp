package realstate.main.dev.app.core.enums.UserRoleType;

public class UserRoleTypeEnumHelper {
    public static int getUserRoleTypeIntValue(UserRoleTypeEnum userRoleTypeEnum) {
        int userType = 0;
        switch (userRoleTypeEnum) {
            case AdminUser:
                userType = 1;
                break;
            case ServiceProviderUser:
                userType = 2;
                break;
            case ServiceSeekerUser:
                userType = 3;
                break;
        }
        return userType;
    }

    public static UserRoleTypeEnum getUserRoleTypeEnumValue(int userRoleType) {
        UserRoleTypeEnum userRoleTypeEnum = UserRoleTypeEnum.NA;
        switch (userRoleType) {
            case 1:
                userRoleTypeEnum = UserRoleTypeEnum.AdminUser;
                break;
            case 2:
                userRoleTypeEnum = UserRoleTypeEnum.ServiceProviderUser;
                break;
            case 3:
                userRoleTypeEnum = UserRoleTypeEnum.ServiceSeekerUser;
                break;
        }
        return userRoleTypeEnum;
    }
}
