package realstate.main.dev.app.core.utils;


import realstate.main.dev.app.core.helpers.JsonHelpers;
import realstate.main.dev.entities.GeneralResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.stream.Collectors;

public class RequestResponseUtils {
    static public String getRequestBody(HttpServletRequest req) throws IOException {
        return req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
    }

    public static void outputResponse(HttpServletResponse response, Object payload, int status, Object... data) {
        try {
            getHeader(response);
            response.setStatus(status);

            if (payload != null) {
                if (payload.getClass() == String.class) {
                    payload = ((String) payload).replaceAll("/[^a-zA-Z ]/g", "");
                }
            } else
                payload = "Unexpected response";
            OutputStream outputStream = response.getOutputStream();
            var finalResponse = new GeneralResponseEntity(status == HttpServletResponse.SC_OK, payload, data);
            outputStream.write(JsonHelpers.getStringJson(finalResponse).getBytes());
            outputStream.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void getHeader(HttpServletResponse response) {
        response.setHeader(
                "Content-Type", "application/json"
        );
    }
}
