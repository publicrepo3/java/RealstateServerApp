package realstate.main.dev.app.core.filters;

import realstate.main.dev.app.core.utils.RequestResponseUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebFilter(asyncSupported = true, urlPatterns = {"/*"})
public class CORSFilter implements Filter {


    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) {
        ThreadLocal<HttpServletResponse> resp = new ThreadLocal<>();
        ThreadLocal<HttpServletRequest> req = new ThreadLocal<>();

        try {
            resp.set((HttpServletResponse) servletResponse);
            req.set((HttpServletRequest) servletRequest);
            ///Add CORS options to preflight response + final response
            ///To prevent CORS error


            resp.get().setHeader("Access-Control-Allow-Origin", "*");
            resp.get().setHeader("Access-Control-Allow-Methods", "*");
            resp.get().setHeader("Access-Control-Max-Age", "3600");
            resp.get().setHeader("Access-Control-Allow-Headers", "x-requested-with, " +
                    "authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN");
            if ("OPTIONS".equalsIgnoreCase(req.get().getMethod())) {
                resp.get().setStatus(HttpServletResponse.SC_OK);
            } else {
                chain.doFilter(servletRequest, servletResponse);
            }

        } catch (Exception e) {
            RequestResponseUtils.outputResponse(resp.get(), e.getLocalizedMessage(),
                    HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        } finally {
            resp.remove();
            req.remove();
        }
    }


    public void init(FilterConfig fConfig) {
        // TODO Auto-generated method stub
    }

}