package realstate.main.dev.app.core.utils;

import realstate.main.dev.entities.ParamEntity;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MultiFormRequestValidator {

    static public boolean validateMultiPartRequest(HttpServletResponse response, List<Part> partList, List<ParamEntity> params) {
        var validated = false;
        try {
            var listOfNotValidatedParts = new ArrayList<Part>();
            var listOfNotValidatedParams = new ArrayList<String>();
            if (partList.stream().anyMatch(Objects::isNull))
                RequestResponseUtils.outputResponse(response, "Null values is prevented.", HttpServletResponse.SC_EXPECTATION_FAILED);
            else {
                partList.forEach((part) -> {

                    try {
                        if (part.getInputStream().read() == 0) {
                            listOfNotValidatedParts.add(part);
                        }
                    } catch (IOException e) {
                        RequestResponseUtils.outputResponse(response, e.getMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
                        e.printStackTrace();
                    }

                });
                params.forEach(param -> {
                    if (param.paramValue == null) {
                        listOfNotValidatedParams.add(param.paramName);
                    }
                });
                if ((long) listOfNotValidatedParts.size() == 0 && listOfNotValidatedParams.size() == 0)
                    validated = true;
                else {
                    var listOfRequiredMessages = new ArrayList<String>();

                    listOfNotValidatedParts.forEach(
                            (part -> listOfRequiredMessages.add(part.getName() + "Is required"))
                    );
                    listOfNotValidatedParams.forEach(param -> listOfRequiredMessages.add(param + "Is required"))
                    ;
                    RequestResponseUtils.outputResponse(response, listOfRequiredMessages.toString(), HttpServletResponse.SC_BAD_REQUEST);

                }
            }


        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
        return validated;
    }

    static public boolean validateGeneralRequest(HttpServletResponse response, List<ParamEntity> params) {
        var validated = false;
        try {
            var listOfNotValidatedParams = new ArrayList<String>();
            if (params.stream().anyMatch(Objects::isNull))
                RequestResponseUtils.outputResponse(response, "Null values is prevented.", HttpServletResponse.SC_EXPECTATION_FAILED);
            else {
                params.forEach(param -> {

                    if (param.paramValue == null || param.paramValue.isEmpty()) {
                        listOfNotValidatedParams.add(param.paramName);
                    }
                });
                if (listOfNotValidatedParams.size() == 0)
                    validated = true;
                else {
                    var listOfRequiredMessages = new ArrayList<String>();
                    listOfNotValidatedParams.forEach(param ->
                            listOfRequiredMessages.add(param + " Is required"));
                    RequestResponseUtils.outputResponse(response, listOfRequiredMessages.toString(), HttpServletResponse.SC_BAD_REQUEST);
                }
            }


        } catch (Exception e) {
            RequestResponseUtils.outputResponse(response, e.getMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
        return validated;
    }
}
