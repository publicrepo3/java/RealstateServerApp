package realstate.main.dev.app.core.utils;

import javax.servlet.ServletContextEvent;
import java.io.InputStream;

public class GetFirebaseInfoUtil {
    public static InputStream getDatabaseKey(ServletContextEvent sce) {
        InputStream in = null;
        try {
            in = sce.getServletContext().getResourceAsStream("/WEB-INF/key.json"); // example

        } finally {
            if (null != in) try {
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return in;
    }
}
