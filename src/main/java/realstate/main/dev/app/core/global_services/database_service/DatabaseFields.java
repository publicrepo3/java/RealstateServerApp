package realstate.main.dev.app.core.global_services.database_service;

public class DatabaseFields {
    public static String userName = "userName";
    public static String userRole = "userRole";
    public static String userId = "userId";
    public static String userPassword = "userPassword";
    public static String userEmail = "userEmail";
    public static String token = "token";
}
