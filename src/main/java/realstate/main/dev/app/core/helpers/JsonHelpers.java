package realstate.main.dev.app.core.helpers;

import com.google.gson.Gson;

import java.util.concurrent.ConcurrentHashMap;

public class JsonHelpers {


    public static String getStringJson(Object list) {
        ///Gson doesn't support serializing anonymous types.
        if (list == null) return null;
        Gson gson = new Gson();
        return gson.toJson(list);
    }
}
