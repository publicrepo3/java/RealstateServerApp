package realstate.main.dev.app.core.global_services.storage_services;

import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import realstate.main.dev.app.core.global_services.ServicesLocator;
import realstate.main.dev.app.core.global_services.database_service.DatabaseStorageService;
import realstate.main.dev.app.core.utils.RequestResponseUtils;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class UploadFilesService {
    private final static Bucket storage = ServicesLocator.databaseService.storage;

    public static URL uploadImages(HttpServletResponse response, Part fileParts, String folderName, String imageName, boolean withSuccessResponse) throws IOException {
        try {
            final var blobId = BlobId.of(DatabaseStorageService.STORAGE_BUCKET, imageName);
            final long duration = 100000L * 100000;
            final var blobString = folderName + "/" + imageName;
            final var blob =
                    storage.create(blobString, fileParts.getInputStream().readAllBytes());

            final var url =
                    blob.signUrl(duration, TimeUnit.DAYS,
                            Storage.SignUrlOption.withV4Signature());
            if (withSuccessResponse)
                RequestResponseUtils.outputResponse(response, "Offer added successfully", HttpServletResponse.SC_OK);
            return url;
        } catch (IOException e) {
            RequestResponseUtils.outputResponse(response, e.getMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
        return null;
    }
}
