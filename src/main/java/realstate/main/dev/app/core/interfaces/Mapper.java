package realstate.main.dev.app.core.interfaces;

public interface Mapper<T, U> {
    U map(T t);
}
