package realstate.main.dev.app.core.utils;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

public class GeneralUtils {

    public static boolean isaTrue(String forRent) {
        return Objects.equals(forRent, "true");
    }

    static final String GENERAL_DATE_TIME_PARSER = "yyyy-MM-dd HH:mm";

    public static Date parsingStringDateTime(String dateTime) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(GENERAL_DATE_TIME_PARSER, Locale.ENGLISH);
        return formatter.parse(dateTime);
    }

    public static String replaceAllSpecialChars(String value) {
        return value.replaceAll("[^a-zA-Z0-9]", " ");
    }

    public static HashMap<String, Object> parsingGeneralObjectToMap(Object obj) throws IllegalAccessException {
        var map = new HashMap<String, Object>();
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            map.put(field.getName(), field.get(obj));

        }
        return map;
    }
}
