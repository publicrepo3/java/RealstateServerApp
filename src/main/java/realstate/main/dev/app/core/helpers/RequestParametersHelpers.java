package realstate.main.dev.app.core.helpers;

public class RequestParametersHelpers {
    public static final String limitTo = "limit";
    public static final String offset = "offset";
    public static final String keyWord = "keyWord";
}
