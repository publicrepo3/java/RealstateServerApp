package realstate.main.dev;

import realstate.main.dev.app.core.global_services.ServicesLocator;
import realstate.main.dev.app.core.utils.GetFirebaseInfoUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.IOException;
import java.io.InputStream;

@WebListener
public class Server implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            intiServices(GetFirebaseInfoUtil.getDatabaseKey(sce));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void intiServices(InputStream kePath) throws IOException {
        try {
            ServicesLocator.initServiceLocator(kePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
