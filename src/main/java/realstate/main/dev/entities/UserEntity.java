package realstate.main.dev.entities;

public class UserEntity {
    final public String userEmail;
    final public String userPassword;
    final public String userName;


    public UserEntity(String userEmail, String userPassword, String userName) {
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userName = userName;

    }
}
