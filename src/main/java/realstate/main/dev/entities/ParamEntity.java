package realstate.main.dev.entities;

public class ParamEntity {
   public final String paramName;
    public  final String paramValue;

    public ParamEntity(String paramName, String paramValue) {
        this.paramName = paramName;
        this.paramValue = paramValue;
    }
}
