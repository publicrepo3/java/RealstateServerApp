package realstate.main.dev.entities;

import realstate.main.dev.api_services.offers.CreateOfferService;
import realstate.main.dev.app.core.helpers.GeneralParamsHelper;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

public class OfferEntity {
    final String offerImageLink;
    final String offerCertificateLink;
    final String offerUserId;
    final boolean isAdminApproved;
    final ServiceTypeEntity offerServiceType;
    final String offerTitle;
    final String offerDesc;
    final boolean isAvailable;
    final String offerAddressDetails;
    final String offerSpace;
    final String offerPrice;
    final String offerRoomsCount;
    final String offerFloor;
    final String offerCladding;

    public OfferEntity(String offerImageLink,
                       String offerCertificateLink,
                       String offerUserId,
                       boolean isAdminApproved,
                       ServiceTypeEntity offerServiceType, String offerTtile, String offerDesc, boolean isAvailable, String offerAddressDetails, String offerSpace, String offerPrice, String offerRoomsCount, String offerFloor, String offerCladding) {
        this.offerImageLink = offerImageLink;
        this.offerCertificateLink = offerCertificateLink;
        this.offerUserId = offerUserId;
        this.isAdminApproved = isAdminApproved;
        this.offerServiceType = offerServiceType;
        this.offerTitle = offerTtile;
        this.offerDesc = offerDesc;
        this.isAvailable = isAvailable;
        this.offerAddressDetails = offerAddressDetails;
        this.offerSpace = offerSpace;
        this.offerPrice = offerPrice;
        this.offerRoomsCount = offerRoomsCount;
        this.offerFloor = offerFloor;
        this.offerCladding = offerCladding;
    }

    public ConcurrentHashMap<String, Object> toMap() {
        return new ConcurrentHashMap<>() {{
            putIfAbsent(GeneralParamsHelper.createdAt, new Date(System.currentTimeMillis()).toString());
            putIfAbsent("offerImageLink", offerImageLink);
            putIfAbsent("offerCertificateLink", offerCertificateLink);
            putIfAbsent("offerUserId", offerUserId);
            putIfAbsent("isAdminApproved", isAdminApproved);
            putIfAbsent("offerServiceType", offerServiceType.toMap());
            putIfAbsent(CreateOfferService.OfferProperties.offerTitle, offerTitle);
            putIfAbsent(CreateOfferService.OfferProperties.offerDesc, offerDesc);
            putIfAbsent(CreateOfferService.OfferProperties.offerIsAvailable, isAvailable);
            putIfAbsent(CreateOfferService.OfferProperties.offerAddressDetails, offerAddressDetails);
            putIfAbsent(CreateOfferService.OfferProperties.offerSpace, offerSpace);
            putIfAbsent(CreateOfferService.OfferProperties.offerPrice, offerPrice);
            putIfAbsent(CreateOfferService.OfferProperties.offerRoomsCount, offerRoomsCount);
            putIfAbsent(CreateOfferService.OfferProperties.offerFloors, offerFloor);
            putIfAbsent(CreateOfferService.OfferProperties.offerCladding, offerCladding);
        }};
    }
}
