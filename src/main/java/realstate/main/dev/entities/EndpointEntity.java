package realstate.main.dev.entities;

public class EndpointEntity {

    final public String name;
    final public String route;

    public EndpointEntity(String name, String route) {
        this.name = name;
        this.route = route;
    }
}
