package realstate.main.dev.entities;

public class OfferSearchEntity {
    public String space;
    public String cladding;
    public String price;
    public String floor;
    public String roomsCount;

    public OfferSearchEntity(String space, String price, String cladding, String floor
            , String roomsCount) {
        this.space = space;
        this.price = price;
        this.cladding = cladding;
        this.floor = floor;
        this.roomsCount = roomsCount;
    }
}
