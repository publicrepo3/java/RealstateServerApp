package realstate.main.dev.entities;

import realstate.main.dev.api_services.apointmetns.AppointmentsService;
import realstate.main.dev.app.core.helpers.GeneralParamsHelper;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

public class AppointmentEntity {
    public final String userId;
    public final String offerId;
    public final String appointmentDate;
    public final String appointmentTime;
    public final boolean isAdminApproved;

    public AppointmentEntity(String userId, String offerId, String appointmentDate, String appointmentTime, boolean isAdminApproved) {
        this.userId = userId;
        this.offerId = offerId;
        this.appointmentDate = appointmentDate;
        this.appointmentTime = appointmentTime;
        this.isAdminApproved = isAdminApproved;
    }

    public ConcurrentHashMap<String, Object> toMap() {
        return new ConcurrentHashMap<>() {{
            putIfAbsent(GeneralParamsHelper.createdAt, new Date(System.currentTimeMillis()).toString());
            putIfAbsent(AppointmentsService.AppointmentsRequestsParams.userId, userId);
            putIfAbsent(AppointmentsService.AppointmentsRequestsParams.offerId, offerId);
            putIfAbsent(AppointmentsService.AppointmentsRequestsParams.appointmentDate, appointmentDate);
            putIfAbsent(AppointmentsService.AppointmentsRequestsParams.appointmentTime, appointmentTime);
            putIfAbsent(AppointmentsService.AppointmentsRequestsParams.appointmentDateTime,
                    String.join(" ", appointmentDate, appointmentTime));
            putIfAbsent(AppointmentsService.AppointmentsRequestsParams.isAdminApproved, isAdminApproved);
        }};
    }
}
