package realstate.main.dev.entities;

import java.util.concurrent.ConcurrentHashMap;

public class ServiceTypeEntity {
    final boolean forSale;
    final boolean forRent;

    public ServiceTypeEntity(boolean forSale, boolean forRent) {
        this.forSale = forSale;
        this.forRent = forRent;
    }

    public Object toMap() {
        return new ConcurrentHashMap<String, Object>() {{
            putIfAbsent("forSale", forSale);
            putIfAbsent("forRent", forRent);
        }};
    }
}
