package realstate.main.dev.entities;

public class GeneralResponseEntity {

    final boolean status;
    final Object response;
    final Object data;

    public GeneralResponseEntity(boolean status, Object response, Object... data) {
        this.status = status;
        this.response = response;
        this.data = data;
    }
}
