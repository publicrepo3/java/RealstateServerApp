package realstate.main.dev.routers.authentication_routes;

import realstate.main.dev.api_services.authenfication.AuthService;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.routers.GeneralRouter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns = {GeneralRouter.AuthenticationRoutes.logInRoute})
public class LoginAccount extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        try {
            AuthService.loginUser(resp, RequestResponseUtils.getRequestBody(req));
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(resp, e.getMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }

    }
}
