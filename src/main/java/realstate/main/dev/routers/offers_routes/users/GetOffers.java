package realstate.main.dev.routers.offers_routes.users;

import realstate.main.dev.api_services.authenfication.AuthService;
import realstate.main.dev.api_services.offers.CreateOfferService;
import realstate.main.dev.api_services.offers.GetOffersService;
import realstate.main.dev.app.core.utils.MultiFormRequestValidator;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.entities.ParamEntity;
import realstate.main.dev.routers.GeneralRouter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class GetOffers {

    @WebServlet(urlPatterns = {GeneralRouter.OffersRoutes.getOfferByUserIdRoute})
    public static class GetOffersByUserId extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
            try {
                var userId = req.getParameter(CreateOfferService.OfferProperties.offerUserId);
                if (AuthService.validateGivenToken(resp, userId))
                    GetOffersService.getOffersByUserId(resp, userId);
            } catch (Exception e) {
                RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
                e.printStackTrace();
            }
        }
    }


    @WebServlet(urlPatterns = {GeneralRouter.OffersRoutes.getOfferByOfferIdRoute})
    public static class GetOffersByOfferId extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
            try {
                var userId = req.getParameter(CreateOfferService.OfferProperties.offerUserId);
                var offerId =
                        req.getParameter(CreateOfferService.OfferProperties.offerId);
                if (AuthService.validateGivenToken(resp, userId))
                    if (MultiFormRequestValidator.validateGeneralRequest(resp, List.of(
                            new ParamEntity(CreateOfferService.OfferProperties.offerUserId, userId),
                            new ParamEntity(CreateOfferService.OfferProperties.offerId, offerId)
                    )))
                        GetOffersService.getOffersByOfferId(resp, offerId);
            } catch (Exception e) {
                RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
                e.printStackTrace();
            }
        }
    }

    @WebServlet(urlPatterns = {GeneralRouter.OffersRoutes.getOfferByUserIdOfferIdRoute})
    public static class GetOffersByUserIdAndOfferId extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
            try {
                var userId = req.getParameter(CreateOfferService.OfferProperties.offerUserId);
                var offerId = req.getParameter(CreateOfferService.OfferProperties.offerId);
                if (MultiFormRequestValidator.validateGeneralRequest(resp, List.of(
                        new ParamEntity(CreateOfferService.OfferProperties.offerUserId, userId),
                        new ParamEntity(CreateOfferService.OfferProperties.offerId, offerId)
                )))
                    if (AuthService.validateGivenToken(resp, userId))
                        GetOffersService.getOffersByUserIdOfferId(resp, userId, offerId);
            } catch (Exception e) {
                RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
                e.printStackTrace();
            }
        }
    }


    @WebServlet(urlPatterns = {GeneralRouter.OffersRoutes.getAllOffersForUsers})
    public static class GetAllOffersForUsers extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
            try {
                GetOffersService.getApprovedAndAvailableOffers(resp);
            } catch (Exception e) {
                RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
                e.printStackTrace();
            }
        }
    }
}