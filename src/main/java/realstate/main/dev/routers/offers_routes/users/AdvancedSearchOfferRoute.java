package realstate.main.dev.routers.offers_routes.users;

import realstate.main.dev.api_services.offers.CreateOfferService;
import realstate.main.dev.api_services.offers.GetOffersService;
import realstate.main.dev.app.core.helpers.RequestParametersHelpers;
import realstate.main.dev.app.core.utils.MultiFormRequestValidator;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.entities.OfferSearchEntity;
import realstate.main.dev.entities.ParamEntity;
import realstate.main.dev.routers.GeneralRouter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {GeneralRouter.OffersRoutes.advancedSearchOfferRoute})
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 3, maxRequestSize = 1024 * 1024 * 5)
public class AdvancedSearchOfferRoute extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            var limitTo = req.getParameter(RequestParametersHelpers.limitTo);
            var offset = req.getParameter(RequestParametersHelpers.offset);

            ///Offer search details
            var space = req.getParameter(CreateOfferService.OfferProperties.offerSpace);
            var price = req.getParameter(CreateOfferService.OfferProperties.offerPrice);
            var roomsCount = req.getParameter(CreateOfferService.OfferProperties.offerRoomsCount);
            var floor = req.getParameter(CreateOfferService.OfferProperties.offerFloors);
            var cladding = req.getParameter(CreateOfferService.OfferProperties.offerCladding);

            if (MultiFormRequestValidator.validateGeneralRequest(resp, List.of(
                    new ParamEntity(RequestParametersHelpers.limitTo, limitTo),
                    new ParamEntity(RequestParametersHelpers.offset, offset)
            )))
                GetOffersService.getAllOffersForSearch(resp, Integer.parseInt(offset),
                        Integer.parseInt(limitTo), new OfferSearchEntity(
                                space, price, cladding, floor, roomsCount
                        ));
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }
}
