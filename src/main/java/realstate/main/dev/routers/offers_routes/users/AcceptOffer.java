package realstate.main.dev.routers.offers_routes.users;

import realstate.main.dev.api_services.offers.EditOffersService;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.routers.GeneralRouter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AcceptOffer {

    @WebServlet(urlPatterns = {GeneralRouter.OffersRoutes.acceptOfferRoute})

    public static class AcceptOfferByUser extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
            try {
                EditOffersService.acceptOfferByUser(resp, req);
            } catch (Exception e) {
                RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
                e.printStackTrace();
            }
        }
    }
}
