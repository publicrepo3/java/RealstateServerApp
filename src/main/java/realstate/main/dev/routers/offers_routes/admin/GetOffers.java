package realstate.main.dev.routers.offers_routes.admin;

import realstate.main.dev.api_services.offers.GetOffersService;
import realstate.main.dev.app.core.helpers.RequestParametersHelpers;
import realstate.main.dev.app.core.utils.MultiFormRequestValidator;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.entities.ParamEntity;
import realstate.main.dev.routers.GeneralRouter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class GetOffers {

    @WebServlet(urlPatterns = {GeneralRouter.OffersRoutes.Admin.getAllOffers})
    static public class GetAllOffers extends HttpServlet {
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
            try {
                var limitTo = req.getParameter(RequestParametersHelpers.limitTo);
                var offset = req.getParameter(RequestParametersHelpers.offset);

                if (MultiFormRequestValidator.validateGeneralRequest(resp, List.of(
                        new ParamEntity(RequestParametersHelpers.limitTo, limitTo),
                        new ParamEntity(RequestParametersHelpers.offset, offset)
                )))
                    GetOffersService.getAllOffers(resp, Integer.parseInt(offset), Integer.parseInt(limitTo));
            } catch (Exception e) {
                RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
                e.printStackTrace();
            }
        }
    }
}
