package realstate.main.dev.routers.offers_routes.users;

import realstate.main.dev.api_services.offers.CreateOfferService;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.routers.GeneralRouter;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "EditOffer", urlPatterns = {GeneralRouter.OffersRoutes.ediOfferRoute})
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 3, maxRequestSize = 1024 * 1024 * 5)
public class EditOffer extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        try {
            CreateOfferService.offerProcessor(resp, req, CreateOfferService.OfferProcessingType.editState);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }
}