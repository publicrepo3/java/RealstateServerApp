package realstate.main.dev.routers.offers_routes.admin;

import realstate.main.dev.api_services.offers.EditOffersService;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.routers.GeneralRouter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditOffers {


    @WebServlet(urlPatterns = {GeneralRouter.OffersRoutes.Admin.approveOfferByAdmin})
    public static class ApproveOffer extends HttpServlet {
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
            try {
                EditOffersService.approveOfferByAdmin(resp, req);
            } catch (Exception e) {
                RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
                e.printStackTrace();
            }
        }
    }
}
