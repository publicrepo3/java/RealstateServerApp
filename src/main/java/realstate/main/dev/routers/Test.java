package realstate.main.dev.routers;

import realstate.main.dev.app.core.utils.GeneralUtils;
import realstate.main.dev.app.core.utils.RequestResponseUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Test extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try {
            var dateTime = req.getParameter("dateTime");
            var parsedDateTime = GeneralUtils.parsingStringDateTime(dateTime);
            RequestResponseUtils.outputResponse(resp, parsedDateTime.toString(), 200);
        } catch (Exception e) {
            RequestResponseUtils.outputResponse(resp, e.getMessage(), HttpServletResponse.SC_EXPECTATION_FAILED);
            e.printStackTrace();
        }
    }


}
