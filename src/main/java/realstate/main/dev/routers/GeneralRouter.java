package realstate.main.dev.routers;

public class GeneralRouter {
    static final private String admin = "/Admin";

    public static class OffersRoutes {
        final public static String advancedSearchOfferRoute = "/AdvancedSearchOfferRoute";
        final public static String ediOfferRoute = "/EditOffer";
        final public static String acceptOfferRoute = "/AcceptOffer";
        final public static String getOfferByUserIdRoute = "/GetOffersByUserId";
        final public static String getOfferByOfferIdRoute = "/GetOffersByOfferId";
        final public static String getOfferByUserIdOfferIdRoute =
                "/GetOffersByUserIdOfferId";
        final public static String createOfferRoute = "/CreateOffer";
        final public static String getAllOffersForUsers =
                "/GetAllApprovedAvailableOffers";

        public static class Admin {
            final public static String getAllOffers = admin + "/GetAllOffers";
            final public static String approveOfferByAdmin = admin + "/ApproveOffer";
        }
    }

    public static class AppointmentsRoutes {
        final public static String makeAppointment = "/MakeAnAppointment";
        final public static String removeAppointment = "/RemoveAppointment";
        final public static String getAppointmentsByUserId = "/GetUserAppointments";
        final public static String getAppointmentsByOfferId = "/GetOfferAppointments";
        final public static String getAppointmentsByOfferUserId =
                "/GetOfferUserAppointments";

        public static class Admin {
            final public static String getAllAppointmentsByAdmin = admin +
                    "/GetAllAppointments";
            final public static String approveAppointmentByAdmin = admin +
                    "/ApproveAppointment";
        }
    }


    public static class AuthenticationRoutes {
        final public static String logInRoute = "/LoginAccount";
        final public static String createAccountRoute = "/CreateAccount";
    }

}
