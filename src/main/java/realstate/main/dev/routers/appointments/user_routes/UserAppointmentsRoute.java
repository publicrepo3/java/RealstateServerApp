package realstate.main.dev.routers.appointments.user_routes;

import realstate.main.dev.api_services.apointmetns.AppointmentsService;
import realstate.main.dev.api_services.authenfication.AuthService;
import realstate.main.dev.app.core.helpers.RequestParametersHelpers;
import realstate.main.dev.app.core.utils.MultiFormRequestValidator;
import realstate.main.dev.app.core.utils.RequestResponseUtils;
import realstate.main.dev.entities.ParamEntity;
import realstate.main.dev.routers.GeneralRouter;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static realstate.main.dev.api_services.apointmetns.AppointmentsService.*;


public class UserAppointmentsRoute {


    @WebServlet(urlPatterns = {GeneralRouter.AppointmentsRoutes.makeAppointment})
    @MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024, maxRequestSize = 1024 * 1024)

    public static class MakeOfferAppointments extends HttpServlet {
        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
            try {
                AppointmentsService.makeAppointments(resp, req);
            } catch (Exception e) {
                RequestResponseUtils.outputResponse(resp, e.getLocalizedMessage(),
                        HttpServletResponse.SC_EXPECTATION_FAILED);
                e.printStackTrace();
            }
        }
    }

    @WebServlet(urlPatterns = {GeneralRouter.AppointmentsRoutes.removeAppointment})

    public static class RemoveAppointment extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
            var userId = req.getParameter(AppointmentsService.AppointmentsRequestsParams.userId);
            var offerId = req.getParameter(AppointmentsService.AppointmentsRequestsParams.offerId);
            var appointmentId =
                    req.getParameter(AppointmentsService.AppointmentsRequestsParams.appointmentId);

            if (MultiFormRequestValidator.validateGeneralRequest(resp, List.of(
                    new ParamEntity(AppointmentsService.AppointmentsRequestsParams.userId, userId),
                    new ParamEntity(AppointmentsService.AppointmentsRequestsParams.offerId, offerId),
                    new ParamEntity(AppointmentsService.AppointmentsRequestsParams.appointmentId, appointmentId
                    )

            )))
                AppointmentsService.removeAppointmentService(resp, userId, appointmentId);
        }
    }


    @WebServlet(urlPatterns = {GeneralRouter.AppointmentsRoutes.getAppointmentsByUserId})

    public static class GetAppointmentsByUserId extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
            var userId =
                    req.getParameter(AppointmentsService.AppointmentsRequestsParams.userId).trim();
            var limitTo = req.getParameter(RequestParametersHelpers.limitTo).trim();
            var offset = req.getParameter(RequestParametersHelpers.offset).trim();

            if (AuthService.validateGivenToken(resp, userId))
                if (MultiFormRequestValidator.validateGeneralRequest(resp, List.of(
                        new ParamEntity(RequestParametersHelpers.limitTo, limitTo),
                        new ParamEntity(RequestParametersHelpers.offset, offset)
                )))
                    getAppointmentsByUserId(resp, userId, limitTo, offset);
        }
    }


    @WebServlet(urlPatterns = {GeneralRouter.AppointmentsRoutes.getAppointmentsByOfferId})

    public static class GetAppointmentsByOfferId extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {


            var appointParams = new AppointmentsService.AppointmentsGettersParams();
            appointParams.limit = req.getParameter(RequestParametersHelpers.limitTo).trim();
            appointParams.offset =
                    req.getParameter(RequestParametersHelpers.offset).trim();
            appointParams.offerId =
                    req.getParameter(AppointmentsService.AppointmentsRequestsParams.offerId).trim();
            appointParams.userId =
                    req.getParameter(AppointmentsService.AppointmentsRequestsParams.userId).trim();

            if (AuthService.validateGivenToken(resp, appointParams.userId))
                if (MultiFormRequestValidator.validateGeneralRequest(resp, List.of(
                        new ParamEntity(RequestParametersHelpers.offset,
                                appointParams.offset),
                        new ParamEntity(RequestParametersHelpers.limitTo,
                                appointParams.limit),
                        new ParamEntity(AppointmentsService.AppointmentsRequestsParams.offerId
                                , appointParams.offerId)
                )))
                    getAppointmentByOfferId(resp, appointParams);
        }
    }


    @WebServlet(urlPatterns =
            {GeneralRouter.AppointmentsRoutes.getAppointmentsByOfferUserId})

    public static class GetAppointmentsByOfferUserId extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

            var userId =
                    req.getParameter(AppointmentsService.AppointmentsRequestsParams.userId).trim();
            var offerId =
                    req.getParameter(AppointmentsService.AppointmentsRequestsParams.offerId).trim();
            var limitTo = req.getParameter(RequestParametersHelpers.limitTo).trim();
            var offset = req.getParameter(RequestParametersHelpers.offset).trim();

            if (AuthService.validateGivenToken(resp, userId))
                if (MultiFormRequestValidator.validateGeneralRequest(resp, List.of(
                        new ParamEntity(RequestParametersHelpers.limitTo, limitTo),
                        new ParamEntity(RequestParametersHelpers.offset, offset),
                        new ParamEntity(AppointmentsService.AppointmentsRequestsParams.offerId
                                , offerId)
                )))
                    getAppointmentByUserIdOfferId(resp, userId, offerId, limitTo, offset);
        }
    }
}


