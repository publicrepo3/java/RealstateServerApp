package realstate.main.dev.routers.appointments.admin_route;

import realstate.main.dev.api_services.apointmetns.AppointmentsService;
import realstate.main.dev.app.core.global_services.database_service.DatabaseFields;
import realstate.main.dev.app.core.helpers.RequestParametersHelpers;
import realstate.main.dev.app.core.utils.MultiFormRequestValidator;
import realstate.main.dev.entities.ParamEntity;
import realstate.main.dev.routers.GeneralRouter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AppointmentsAdminRoute {
    @WebServlet(urlPatterns = {GeneralRouter.AppointmentsRoutes.Admin.getAllAppointmentsByAdmin})

    public static class GetAppointmentsByAdmin extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
            var limit = req.getParameter(RequestParametersHelpers.limitTo).trim();
            var offset =
                    req.getParameter(RequestParametersHelpers.offset).trim();
            var userId = req.getParameter(DatabaseFields.userId);
            if (MultiFormRequestValidator.validateGeneralRequest(resp, List.of(
                    new ParamEntity(RequestParametersHelpers.offset,
                            offset),
                    new ParamEntity(RequestParametersHelpers.limitTo,
                            limit),
                    new ParamEntity(DatabaseFields.userId,
                            userId)
            )))
                AppointmentsService.Admin.getAppointmentsByAdmin(resp, offset, limit, userId);
        }
    }


    @WebServlet(urlPatterns = {GeneralRouter.AppointmentsRoutes.Admin.approveAppointmentByAdmin})

    public static class ApproveAppointmentsByAdmin extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

            var userId = req.getParameter(DatabaseFields.userId);
            var appointmentId =
                    req.getParameter(AppointmentsService.AppointmentsRequestsParams.appointmentId);
            var isAdminApproved =
                    req.getParameter(AppointmentsService.AppointmentsRequestsParams.isAdminApproved);

            if (MultiFormRequestValidator.validateGeneralRequest(resp, List.of(
                    new ParamEntity(AppointmentsService.AppointmentsRequestsParams.appointmentId,
                            appointmentId),
                    new ParamEntity(DatabaseFields.userId,
                            userId),
                    new ParamEntity(AppointmentsService.AppointmentsRequestsParams.isAdminApproved,
                            isAdminApproved)
            )))
                AppointmentsService.Admin.approveAppointmentService(resp, userId,
                        appointmentId, isAdminApproved);
        }
    }
}
